// Locals librairies
#include "./rules.h"

// Standart librairies
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

int getNbRow(char *fileName) {

    int row = 0;
    char line[100];
    const char s[] = " ";

    FILE *file;
    file=fopen(fileName,"r");
    if(file==NULL) {
        printf("File \"%s\" does not exist!!!\n","params.txt");
        return 0;
    }

    while (fgets(line, sizeof(line), file)) {
            if (line == NULL) {
                break;
            } else {
                
                char *p = strtok(line, " ");
                if(atoi(line)> 0)
                    row++;

                /* walk through other tokens */
                while( p != NULL ) {
                    
                    if (atoi(p) != 0) {
                        p = strtok(NULL, s);
                        
                    } else {
                        
                        p = strtok(NULL, s);
                    }
                   
                }
            }
        }
        fclose(file);
    return row;
}
char* readParams(int rowParam, char **param, int row) {
    FILE *file;
    file=fopen("params.txt","r");
    if(file==NULL) {
        printf("File \"%s\" does not exist!!!\n","params.txt");
        return 0;
    }

    char line[100];
    const char s[] = " ";
    int value;

    while (fgets(line, sizeof(line), file)) {
            if (line == NULL) {
                break;
            } else {

                if(atoi(line) != 0)
                {
                    
                    if(row == rowParam ){
                        char* test = line;

                        return test;
                    }
                    else{
                        row++;
                    }
                }   
                char *p = strtok(line, " ");

                /* walk through other tokens */
                while( p != NULL ) {
                    if (atoi(p) != 0) {
                        p = strtok(NULL, s);

                    } else {
                        p = strtok(NULL, s);
                    }
                    
                }
            }
        }
        fclose(file);
} 
char* readRules(int rowRules, char **rules, int row) {
    FILE *file;
    file=fopen("params.txt","r");
    if(file==NULL) {
        printf("File \"%s\" does not exist!!!\n","params.txt");
        return 0;
    }

    char line[100];
    const char s[] = " ";
    int value;

    while (fgets(line, sizeof(line), file)) {
            if (line == NULL) {
                break;
            } else {

                if(atoi(line) != 0)
                {
                    
                    if(row == rowRules ){
                        char* rules = line;
                        return rules;
                    }
                    else{
                        row++;
                    }
                }   
                char *p = strtok(line, " ");

                /* walk through other tokens */
                while( p != NULL ) {
                    if (atoi(p) != 0) {
                        p = strtok(NULL, s);

                    } else {
                        p = strtok(NULL, s);
                    }
                    
                }
            }
        }
        fclose(file);
}

int main (void) {
    
    char *param = "";
    char *rules = "";
    int nbrowParams = getNbRow ("params.txt");
    int nbrowRules = getNbRow("rules.txt");

    printf("%d\n",nbrowRules);

    char *test = malloc (999);
    int tab[strlen (test)];
    char *tabParams[nbrowParams];
    int i, j;
    

    //static char* testTab2d[10][256];
    
    test = readParams(nbrowParams, &param, 1);
    printf("%s\n",test);

   
    /*
    test = readParams(nbrow, &param, 4);
        for ( i = 0; i < strlen(test)-1; i += 2) {
            tab[i] = test[i]-48;
        }
    
    
    /*for (i = nbrow; i >= 1 ; i--) {
        test = readParams(nbrow, &param, i);
        for ( i = 0; i < strlen(test)-1; i += 2) {
            tab[i] = test[i]-48;
        }
        printf("\n");
    }
    
    for ( i = 0; i < strlen(test)-1; i += 2) {
        printf("%d ", tab[i]);
    }
    */
    

    return 1;
}

/*
int main(void) {
    int tab1[3] = {3,1,2};
    int tab2[3] = {1,2,3};
    
    char same = 'Y';
    qsort( tab1, 3, sizeof(int), middle );
    
    for (int i = 0; i < 3; i++) {
        if(tab1[i] != tab2[i]) {
            same = 'N';
        }
    }
    printf("%c\n", same);
    return EXIT_SUCCESS;
}*/