#ifndef RULES_H_
#define RULES_H_

typedef struct _Chains Chains;
/**
 * @brief Read the parameters file and return the line wanted on the file
 * 
 * @param rowParam 
 * @param param 
 * @param row 
 * @return char* 
 */
extern char* readParams (int rowParam, char **param, int row);

/**
 * @brief Read the Rules file and return the line wanted on the file
 * 
 * @param rowRules 
 * @param rules 
 * @param row 
 * @return char* 
 */
extern char* readRules(int rowRules, char **rules, int row);

/**
 * @brief Get the Nb Row object
 * 
 * @param fileName 
 * @return int 
 */
extern int getNbRow(char *fileName);


#endif 
